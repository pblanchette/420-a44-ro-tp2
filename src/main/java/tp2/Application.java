package tp2;

import java.io.IOException;
import java.util.Scanner;
import tp2.mock.SalleServiceMock;
import tp2.services.SalleService;
import tp2.vues.SalleTerminal;

public class Application {
  public static void main(String[] args) {
    SalleService service = new SalleServiceMock(); // À changer
    Scanner sc = new Scanner(System.in);
    SalleTerminal terminal = new SalleTerminal(service, sc);
    try {
      terminal.menu();
    } catch (IOException e) {
      System.out.println("Erreur fatal: problème avec le fichier de sauvegarde.");
      System.exit(1);
    } catch (Exception e) {
      System.out.println("Erreur fatal: erreur inattendue.");
      System.exit(1);
    }
    System.exit(0);
  }
}
