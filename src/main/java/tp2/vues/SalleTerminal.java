package tp2.vues;

import java.io.IOException;
import java.util.InputMismatchException;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;
import tp2.entities.Salle;
import tp2.services.SalleService;

public class SalleTerminal {

  private static final String ENTETE_SALLE = "ID   CODE CAPACITE DESCRIPTION";

  private SalleService service;

  private Scanner sc;

  public SalleTerminal(SalleService service, Scanner sc) {
    super();
    this.service = service;
    this.sc = sc;
  }

  public void menu() throws IOException {
    String choix;
    boolean valide;
    do {
      System.out.println("Menu de gestion de salle:");
      System.out.println("l: Liste des salles");
      System.out.println("n: Créer une nouvelle salle");
      System.out.println("c: Consulter une salle");
      System.out.println("m: Modifier une salle");
      System.out.println("s: Supprimer une salle");
      System.out.println("q: Quitter");
      do {
        valide = true;
        System.out.print("Choix:");
        choix = sc.next();
        switch (choix) {
          case "l":
            listeSalles();
            break;
          case "n":
            creerSalle();
            break;
          case "c":
            consulterSalle();
            break;
          case "m":
            modifierSalle();
            break;
          case "s":
            supprimerSalle();
            break;
          case "q":
            break;
          default:
            System.out.println("Le menu ne contient pas d'option: " + choix);
            valide = false;
        }
      } while (!valide);
    } while (!choix.equals("q"));

  }

  private void supprimerSalle() throws IOException {
    int id;
    try {
      System.out.print("Entrez le ID de la salle à supprimer: ");
      id = entreInt();
      service.supprimerSalle(id);
      System.out.println("La salle a été supprimée!");
    } catch (NoSuchElementException e) {
      System.out.println("Erreur: " + e.getMessage());
    }
  }

  private void modifierSalle() throws IOException {
    System.out.println("Modifier d'une nouvelle salle");
    String code;
    int capacite;
    String description;
    int id;
    Salle salle;
    try {
      System.out.print("Entrez le ID de la salle à consulter: ");
      id = entreInt();
      System.out.print("Entrer le nouveau code: ");
      code = sc.next();
      System.out.print("Entrer la nouvelle capacite: ");
      capacite = entreInt();
      System.out.print("Entrer la nouvelle description: ");
      description = sc.next();
      salle = service.modifierSalle(id, code, capacite, description);
      System.out.println("La salle a été modifiée!");
      System.out.println(ENTETE_SALLE);
      System.out.println(salleToString(salle));
    } catch (IllegalArgumentException | NoSuchElementException e) {
      System.out.println("Erreur: " + e.getMessage());
    }
  }

  private void consulterSalle() throws IOException {
    int id;
    Salle salle;
    try {
      System.out.print("Entrez le ID de la salle à consulter: ");
      id = entreInt();
      salle = service.trouverSalleParId(id);
      System.out.println(ENTETE_SALLE);
      System.out.println(salleToString(salle));
    } catch (NoSuchElementException e) {
      System.out.println("Erreur: " + e.getMessage());
    }
  }

  private void creerSalle() throws IOException {
    System.out.println("Création d'une nouvelle salle");
    String code;
    int capacite;
    String description;
    try {
      System.out.print("Entrer le code: ");
      code = sc.next();
      System.out.print("Entrer la capacite: ");
      capacite = entreInt();
      System.out.print("Entrer la description: ");
      description = sc.next();
      Salle salle = service.creerSalle(code, capacite, description);
      System.out.println("La salle a été crée!");
      System.out.println(ENTETE_SALLE);
      System.out.println(salleToString(salle));
    } catch (IllegalArgumentException e) {
      System.out.println("Erreur: " + e.getMessage());
    }
  }

  private void listeSalles() throws IOException {
    List<Salle> salles = service.trouverListeSalle();
    System.out.println(ENTETE_SALLE);
    if (salles.isEmpty()) {
      System.out.println("Il n'y a pas de salle");
    } else {
      salles.forEach(salle -> System.out.println(salleToString(salle)));
    }
  }

  private String salleToString(Salle salle) {
    return String.format("%1$-4d %2$s %3$-8d %4$s", salle.getId(), salle.getCode(),
        salle.getCapacite(), salle.getDescription());
  }

  private int entreInt() {
    boolean valide = false;
    int entre = 0;
    do {
      try {
        entre = sc.nextInt();
        valide = true;
      } catch (InputMismatchException e) {
        sc.nextLine();
        System.out.print("Entré invalide!\nVeuillez saisir un nombre entier:");
      }
    } while (!valide);
    return entre;
  }
}
