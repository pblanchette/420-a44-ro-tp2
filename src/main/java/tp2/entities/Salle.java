package tp2.entities;

import java.util.concurrent.atomic.AtomicInteger;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Salle {

  private static AtomicInteger sequenceId = new AtomicInteger(0);

  private int id;
  private String code;
  private int capacite;
  private String description;

  public Salle(String code, int capacite, String description) {
    super();
    this.id = sequenceId.incrementAndGet();
    this.code = code;
    this.capacite = capacite;
    this.description = description;
  }

}
