package tp2.services;

import java.io.IOException;
import java.util.List;
import tp2.entities.Salle;

public class SalleService {

  public Salle creerSalle(String code, int capacite, String description) throws IOException {
    // À compléter
    return null;
  }

  public void supprimerSalle(int id) throws IOException {
    // À compléter
  }

  public Salle modifierSalle(int id, String code, int capacite, String description)
      throws IOException {
    // À compléter
    return null;
  }

  public Salle trouverSalleParId(int id) throws IOException {
    // À compléter
    return null;
  }

  public List<Salle> trouverListeSalle() throws IOException {
    // À compléter
    return null;
  }
}
