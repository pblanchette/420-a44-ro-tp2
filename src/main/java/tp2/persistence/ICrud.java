package tp2.persistence;

import java.io.IOException;
import java.util.List;

public interface ICrud<T> {

  public T create(T o) throws IOException;

  public void delete(int id) throws IOException;

  public T update(T o) throws IOException;

  public T findById(int id) throws IOException;

  public List<T> findAll() throws IOException;

}
