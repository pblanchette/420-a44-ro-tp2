package tp2.mock;

import java.util.ArrayList;
import java.util.List;
import lombok.Getter;
import tp2.entities.Salle;
import tp2.services.SalleService;

@Getter
public class SalleServiceMock extends SalleService {

  private String spy;

  private Salle salle;

  public SalleServiceMock() {
    Salle salle = new Salle();
    salle.setId(1);
    salle.setCode("B134");
    salle.setCapacite(30);
    salle.setDescription("Informatique");
    this.salle = salle;
  }

  @Override
  public Salle creerSalle(String code, int capacite, String description) {
    spy = "" + code + "::" + capacite + "::" + description;
    return salle;
  }

  @Override
  public void supprimerSalle(int id) {
    spy = "" + id;
  }

  @Override
  public Salle modifierSalle(int id, String code, int capacite, String description) {
    spy = "" + id + "::" + code + "::" + capacite + "::" + description;
    return salle;
  }

  @Override
  public Salle trouverSalleParId(int id) {
    spy = "" + id;
    return salle;
  }

  @Override
  public List<Salle> trouverListeSalle() {
    List<Salle> salles = new ArrayList<Salle>();
    salles.add(salle);
    return salles;
  }

}
